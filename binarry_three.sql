CREATE TABLE `binary_three` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(100) NOT NULL,
	`credits_left` INT(11) NOT NULL DEFAULT ''0'',
	`credits_right` INT(11) NOT NULL DEFAULT ''0'',
	`parent_id` INT(11) NULL DEFAULT NULL,
	`site` ENUM(''R'',''L'') NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_binary_three_parent_id` (`parent_id`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
;
ALTER TABLE `binary_three`
	ADD CONSTRAINT `FK_binary_three` FOREIGN KEY (`parent_id`) REFERENCES `binary` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;