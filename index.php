<?php

include_once 'Service/BinaryTreeLoadService.php';
include_once 'Service/HTMLService.php';

$htmlService = new HTMLService();

?>
<html>
    <thead>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="web/style.css">

    </thead>
    <body>
        <div class="text-center ">
            <div class="container">
                <?php $htmlService->renderHTMLThee()?>
            </div>
        </div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type = "text/javascript" src = "web/javaScript.js" ></script>
</html>
