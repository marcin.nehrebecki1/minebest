<?php

include_once 'BaseLoadService.php';

class BinaryTreeLoadService extends BaseLoadService
{
    private $stop;
    private $number;
    private $nameService;

    public function __construct(int $numberBranch)
    {
        $this->nameService = new NameService();
        $this->stop = $numberBranch;
        $this->number = 0;
    }

    /**
     * @param bool $first
     * @return BranchTreeLoadService
     */
    public function createAllBranchTree($first = true): BranchTreeLoadService
    {
        return $this->createBranch($this->nameService->generateMD5(), $first);
    }

    /**
     * @param string $name
     * @param bool $first
     * @return BranchTreeLoadService
     */
    private function createBranch(string $name, bool $first = false): BranchTreeLoadService
    {
        if (!$first) {
            $this->number++;
        }
        $newBranchTree = new BranchTreeLoadService($name, $first);
        if ($this->number <= $this->stop) {
            $this->createSiteBranch($newBranchTree, 'Left');
            $this->createSiteBranch($newBranchTree, 'Right');
        }
        return $newBranchTree;
    }

    /**
     * @param BranchTreeLoadService $newBranchTree
     * @param string $site Right|Left
     */
    private function createSiteBranch(BranchTreeLoadService $newBranchTree, string $site)
    {
        if ($newBranchTree->{'getCredits' . $site}() !== 0) {
            $newBranchTree->{'setBranch' . $site}(
                $this->createBranch($this->nameService->generateMD5())
            );
        }
    }
}