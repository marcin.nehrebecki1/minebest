<?php

include_once 'BaseLoadService.php';

class BranchTreeLoadService extends BaseLoadService
{
    private $userName;
    private $creditsLeft;
    private $creditsRight;
    private $branchLeft;
    private $branchRight;
    private $first;

    public function __construct(string $userName, bool $first = false)
    {
        $this->setFirst($first);
        $this->setCreditsLeft(SimpleFunctionService::generateRand());
        $this->setCreditsRight(SimpleFunctionService::generateRand());
        $this->setUserName($userName);

    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @param int $creditsLeft
     */
    public function setCreditsLeft(int $creditsLeft): void
    {
        $this->creditsLeft = $this->setCredits($creditsLeft);
    }

    /**
     * @return int
     */
    public function getCreditsLeft(): int
    {
        return $this->creditsLeft;
    }

    /**
     * @return int
     */
    public function getCreditsRight(): int
    {
        return $this->creditsRight;
    }

    /**
     * @param int $creditsRight
     */
    public function setCreditsRight(int $creditsRight): void
    {
        $this->creditsRight = $this->setCredits($creditsRight);
    }

    /**
     * @param bool $first
     */
    public function setFirst(bool $first): void
    {
        $this->first = $first;
    }

    /**
     * @return bool
     */
    public function getFirst(): bool
    {
        return $this->first;
    }

    /**
     * @param int $creditsLeft
     * @return int
     */
    private function setCredits(int $creditsLeft): int
    {
        $credits = 0;
        if ($this->getFirst() || SimpleFunctionService::generateRandomBool()) {
            $credits = $creditsLeft;
        }
        return $credits;
    }

    /**
     * @param BranchTreeLoadService $branch
     */
    public function setBranchLeft(BranchTreeLoadService $branch): void
    {
        $this->branchLeft = $branch;
    }

    /**
     * @param BranchTreeLoadService $branch
     */
    public function setBranchRight(BranchTreeLoadService $branch): void
    {
        $this->branchRight = $branch;
    }

    /**
     * @return BranchTreeLoadService|null
     */
    public function getBranchLeft(): ?BranchTreeLoadService
    {
        return $this->branchLeft;
    }

    /**
     * @return BranchTreeLoadService|null
     */
    public function getBranchRight(): ?BranchTreeLoadService
    {
        return $this->branchRight;
    }
}