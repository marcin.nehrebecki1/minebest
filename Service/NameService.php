<?php

class NameService
{
    private $lastName;

    public function __construct()
    {
        $this->setLastName('');
    }

    /**
     * @return string
     */
    public function generateMD5(): string
    {
        $md5 = substr(md5(SimpleFunctionService::generateRand() . $this->getLastName()), 0, 10);
        $this->setLastName($md5);
        return $md5;
    }

    /**
     * @return string|null
     */
    private function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    private function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }


}