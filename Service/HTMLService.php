<?php

class HTMLService
{

    public function renderHTMLThee(): void
    {
        $tree = new BinaryTreeLoadService(1000);
        $tree = $tree->createAllBranchTree();
        print $this->showBranch($tree);
    }

    /**
     * @param BranchTreeLoadService $branch
     * @return string
     */
    private function showBranch(BranchTreeLoadService $branch): string
    {
        $branchRight = $branchLeft = $classNested = $caretLeft = $caretRight = '';
        if ($branch->getBranchLeft() instanceof BranchTreeLoadService) {
            $branchLeft = $this->showBranch($branch->getBranchLeft());
        }

        if ($branch->getBranchRight() instanceof BranchTreeLoadService) {
            $branchRight = $this->showBranch($branch->getBranchRight());
        }

        if (!$branch->getFirst()) {
            $classNested = 'class="nested"';
        }

        if ($branch->getCreditsLeft() != 0) {
            $caretLeft = 'caret';
        }

        if ($branch->getCreditsRight() != 0) {
            $caretRight = 'caret';
        }

        $html ='
                <ul id="myUL">
                    <li>Name: ' . $branch->getUserName() . '</li>
                    <hr>
                    <ul '. $classNested  .'>
                        <li class="liClass"><span class="'. $caretLeft .'">Left: '. $branch->getCreditsLeft() .'</span> '. $branchLeft .'</li>
                        <li class="liClass"><span class="'. $caretRight .'">Right: '. $branch->getCreditsRight() .'</span> '. $branchRight .'</li>
                    </ul>
                </ul>
                
        ';
        return $html;
    }
}