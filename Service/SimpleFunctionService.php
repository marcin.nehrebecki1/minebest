<?php

class SimpleFunctionService
{

    /**
     * @return int
     */
    public static function generateRand(): int
    {
        return (int)rand(0, 1000);
    }

    /**
     * @return bool
     */
    public static function generateRandomBool(): bool
    {
        return ( rand(0,1) == 1 )? true : false;
    }
}